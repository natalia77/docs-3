The YouGiver API provides an opportunity to secure the Developer’s app from receiving event notifications from third-party services as if they were notifications from YouGiver.

For this, a Developer can specify a personal header (**notificationsHeaderKey**) and its value (**notificationsHeaderValue**) in the "Keys" section of the "Settings. General" section in Developer's personal account.

> These parameters are **NOT** obligatory to configure, but the YouGiver team strongly recommends using this tool to improve the security of the systems interaction.

<p align="center">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/security_key.png" alt="title text"/>
</p>

The header and the key specified by the Developer will be passed to the side of the Developer’s app in the structure of each sent notification. Analyzing these two parameters, the developer for his part can determine whether this notification was received from YouGiver, or from any third-party service.

**If the header and the key of the notification are different from those specified in Developer's personal account - you should not use and transmit this notification any further!**